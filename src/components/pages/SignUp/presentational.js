import React from 'react'
import SignUpForm from 'components/briks/SignUpForm'

const Signup = ({
  isAuthenticated,
  userName,
  mainMessage,
}) => (
  <div>
    {isAuthenticated
      ? <div>{`ciao ${userName}! L'iscrizione è avvenuta con successo :)`}</div>
      : (
        <div>
          {mainMessage}
          <SignUpForm />
        </div>
      )
    }
  </div>
)

Signup.propTypes = {
  isAuthenticated: React.PropTypes.bool.isRequired,
  mainMessage: React.PropTypes.string,
  userName: React.PropTypes.string,
}

export default Signup
