import React from 'react'
import Input from 'react-toolbox/lib/input'
import style from './style.css'


const InputText = ({
  disabled,
  error,
  floating,
  hint,
  icon,
  label,
  maxLength,
  multiline,
  name,
  rows,
  status,
  required,
  type,
  value,
  onBlur,
  onChange,
  onKeyPress,
  onFocus,
}) => (
  <Input
    className={[style.theme, style[status]].join(' ')}
    disabled={disabled}
    error={error}
    floating={floating}
    hint={hint}
    icon={icon}
    label={label}
    maxLength={maxLength}
    multiline={multiline}
    name={name}
    rows={rows}
    required={required}
    type={type || 'text'}
    value={value || ''}
    onBlur={(event, value) => (onBlur ? onBlur(event, value) : () => {})}
    onChange={(event, value) => (onChange ? onChange(event, value) : () => {})}
    onKeyPress={(event, value) => (onKeyPress ? onKeyPress(event, value) : () => {})}
    onFocus={(event, value) => (onFocus ? onFocus(event, value) : () => {})}
  />
)

InputText.propTypes = {
  disabled: React.PropTypes.bool,
  error: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.node,
  ]),
  floating: React.PropTypes.bool,
  hint: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.node,
  ]),
  icon: React.PropTypes.string,
  label: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.node,
  ]),
  maxLength: React.PropTypes.number,
  multiline: React.PropTypes.bool,
  name: React.PropTypes.string,
  rows: React.PropTypes.number,
  required: React.PropTypes.bool,
  status: React.PropTypes.string,
  type: React.PropTypes.string,
  value: React.PropTypes.string,
  onBlur: React.PropTypes.func,
  onChange: React.PropTypes.func,
  onKeyPress: React.PropTypes.func,
  onFocus: React.PropTypes.func,
}

export default InputText
