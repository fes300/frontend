import React from 'react';
import expect from 'expect';
import TestUtils from 'react-addons-test-utils';
import {createRenderer, Simulate} from 'react-addons-test-utils';

import InputText from './component';


describe('InputText', function() {

  it('renders with props {name}', function() {

    const jsx = (
      <InputText
        name='input-text-name'/>
    );

    const expected = (
      <input
        type='text'
        name='input-text-name'/>
    );

    const render = function() {
      const renderer = createRenderer();
      renderer.render(jsx);
      return renderer.getRenderOutput();
    }

    const actual = render();

    expect(actual.type).toBe(expected.type);
    expect(actual.props.type).toEqual(expected.props.type);
    expect(actual.props.name).toEqual(expected.props.name);
    expect(actual.props.placeholder).toEqual(undefined);
    expect(actual.props.value).toEqual(undefined);
  });


  it('renders with props {name, placeholder}', function() {

    const jsx = (
      <InputText
        name='input-text-name'
        placeholder='set your value'/>
    );

    const expected = (
      <input
        type='text'
        name='input-text-name'
        placeholder='set your value'/>
    );

    const render = function() {
      const renderer = createRenderer();
      renderer.render(jsx);
      return renderer.getRenderOutput();
    }

    const actual = render();

    expect(actual.type).toBe(expected.type);
    expect(actual.props.type).toEqual(expected.props.type);
    expect(actual.props.name).toEqual(expected.props.name);
    expect(actual.props.placeholder).toEqual(expected.props.placeholder);
    expect(actual.props.value).toEqual(undefined);
  });


  it('renders with props {name, placeholder, value}', function() {

    const jsx = (
      <InputText
        name='input-text-name'
        placeholder='set your value'
        value='Alice'/>
    );

    const expected = (
      <input
        type='text'
        name='input-text-name'
        placeholder='set your value'
        value='Alice'/>
    );

    const render = function() {
      const renderer = createRenderer();
      renderer.render(jsx);
      return renderer.getRenderOutput();
    }

    const actual = render();

    expect(actual.type).toBe(expected.type);
    expect(actual.props.type).toEqual(expected.props.type);
    expect(actual.props.name).toEqual(expected.props.name);
    expect(actual.props.value).toEqual(expected.props.value);
  });


  it('changes props {value} (controlled component)', function() {

    const jsx = (
      <InputText
        refs='input'
        value='Alice'/>
    );

    const node = TestUtils.renderIntoDocument(jsx);
    const component = TestUtils.findRenderedDOMComponentWithTag(node, 'input');

    const value = 'Alice is changed';
    Simulate.change(component, {target: {value: value}})

    expect(component.value).toEqual(value);
  });

});