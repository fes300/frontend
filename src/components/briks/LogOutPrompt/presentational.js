import React from 'react'
import { Button } from 'react-toolbox/lib/button'
import style from './style.css'


const LoginForm = ({
  userName,
  onClick,
}) => (
  <div>
    <span>ciao {userName}, sei sicuro di voler fare logout?</span>
    <Button
      className={style.logoutButton}
      label="logout"
      raised
      onClick={onClick}
    />
  </div>
)

LoginForm.propTypes = {
  userName: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func.isRequired,
}

export default LoginForm
