import { connect } from 'react-redux'
import { authRequest, authSuccess, authFailure } from 'constants/actionCreators'
import { validatePassword } from 'constants/validations'
import Presentational from './presentational'


const mapStateToProps = state => state.auth.toObject()

const mapDispatchToProps = dispatch => ({
  onSubmit: (event) => {
    event.preventDefault()
    const data = new FormData(event.target) // eslint-disable-line

    // validate userName
    const userName = data.get('userName')
    if (!userName) return dispatch(authFailure({
      userNameState: {
        message: 'il campo non può essere lasciato vuoto',
        status: 'error',
        error: true,
      },
      mainMessage: 'il campo "nome utente" non può essere lasciato vuoto',
    }))

    // validate password
    const password = data.get('password')
    const message = validatePassword(password)
    if (message) return dispatch(authFailure({
      passwordState: {
        message,
        status: 'error',
        error: true,
      },
      mainMessage: message,
    }))

    return dispatch(authRequest({
      userNameState: {
        message: 'lo userName è valido',
        status: 'success',
        value: userName,
        error: false,
      },
      passwordState: {
        message: 'la password è valida',
        status: 'success',
        value: password,
        error: false,
      },
      mainMessage: null,
    }))
  },

  validateUserName: value => dispatch(authSuccess({
    userNameState: {
      message: 'lo userName è valido',
      status: 'success',
      value,
      error: false,
    },
    mainMessage: null,
  })),

  validatePassword: (value) => {
    const message = validatePassword(value)
    if (message) {
      return dispatch(authFailure({
        passwordState: {
          message,
          status: 'error',
          value,
          error: true,
        },
        mainMessage: null,
      }))
    }
    return dispatch(authSuccess({
      passwordState: {
        message: 'la password è valida',
        status: 'success',
        value,
        error: false,
      },
      mainMessage: null,
    }))
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Presentational)
