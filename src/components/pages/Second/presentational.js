import React from 'react'
import { requireAuthentication } from 'components/containers/Auth'
import UnauthComponent from './unauthComponent'


const Component = ({ userName }) => (
  <div>ciao {userName}!</div>
)

Component.propTypes = {
  userName: React.PropTypes.string.isRequired,
}

export default requireAuthentication({
  Component,
  UnauthComponent,
  showLogin: true,
})
