import { darkGray, borderGray, lightRed, lightGreen } from 'constants/style/variables'


export default {
  error: {
    borderColor: lightRed,
  },

  success: {
    borderColor: lightGreen,
  },
  // // hide the arrows on number input
  // &[type="number"]::-webkit-outer-spin-button,
  // &[type="number"]::-webkit-inner-spin-button {
  //     -webkit-appearance: none,
  //     margin: 0,
  // }
  // &[type="number"] {
  //     -moz-appearance: textfield,
  // }
}
