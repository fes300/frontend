const path = require('path')
const webpack = require('webpack')
const OfflinePlugin = require('offline-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')


module.exports = {
  devtool: 'inline-source-map',

  entry: [
    'babel-polyfill',
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
    path.resolve(__dirname, 'src', 'components', 'containers', 'App', 'index.js'),
    path.resolve(__dirname, 'src', 'index.js'),
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },

  context: path.resolve(__dirname, 'src'),

  devServer: {
    hot: true,
    // enable HMR on the server

    contentBase: path.resolve(__dirname, 'dist'),
    // match the output path

    inline: true,
    publicPath: '/',
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
      },
    }),

    new webpack.HotModuleReplacementPlugin(),
    // enable HMR globally

    new webpack.NamedModulesPlugin(),
    // prints more readable module names in the browser console on HMR updates

    new HtmlWebpackPlugin({
      template: 'index.html',
    }),

    new OfflinePlugin({
      responseStrategy: 'network-first',
      updateStrategy: 'all',
      AppCache: false,
    }),
  ],

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          plugins: ['react-hot-loader/babel'],
        },
      },
      {
        test: /\.css$/,
        include: [
          /(node_modules)\/react-toolbox/,
          path.resolve(__dirname, 'src'),
        ],
        loaders: [
          'style-loader?singleton',
          'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
          'postcss-loader',
        ],
      },
      {
        test: /\.json$/,
        use: 'json-loader',
      },
    ],
  },

  resolve: {
    modules: [
      path.resolve(__dirname, './src'),
      'node_modules',
      path.resolve(__dirname, './node_modules'),
    ],
  },
}
