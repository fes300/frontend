import React from 'react'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

import { storiesOf } from '@kadira/storybook'
import InputText from './'


const store = createStore(() => ({}))

storiesOf('InputText', InputText)

  .add('Base', () => (
    <Provider store={store}>
      <InputText name="atom-input-text-1" />
    </Provider>
  ))

  .add('With Placeholder', () => (
    <Provider store={store}>
      <InputText name="atom-input-text-2" placeholder="Write your surname" />
    </Provider>
  ))

  .add('With value', () => (
    <Provider store={store}>
      <InputText name="atom-input-text-3" value="Wonderland" />
    </Provider>
  ))

  .add('Error', () => (
    <Provider store={store}>
      <InputText
        name="atom-input-text-4"
        value="Wonderland does not exist :("
        className="error"
      />
    </Provider>
  ))
