# NEW Redux Customer Journey

You can find a basic wiki of some of the technologies used [here](http://fes300.github.io/).

## Prerequisite Technologies
### Linux
* *node.js*
* *npm*
* *mocha*
* *eslint* -if you use Atom install also `linter-eslint` atom package
* *git*


## Installation
To download dependencies run `npm i` from the project root.
Once the command is finished, you can run the following CLI commands from the project root:

```

$ npm run startAuto
* run a local server on port 4000 serving autoCJ (no bundle created)

$ npm run startMoto
* run a local server on port 4000 serving motoCJ (no bundle created)

$ npm run build
* creates ./distAuto/old, ./distAuto/bundle.js and ./distAuto/bundle.css (if not there already)
* move old dist versions to '/old' directory (if there is any)
* run the ./update.sh file in the repository root
* creates bundles of all js and css called respectively '$GIT_VERSION-bundle.js' and '$GIT_VERSION-bundle.css' in ./distAuto directory

$ npm [--browser="chrome"] [--host="http://192.168.1.4:4000"] test
* start a mocha test using Selenium WebDriver for NodeJs (entry point './test.js'). Chrome driver is installed automatically as a npm package, if you want to run the script on other browsers you need to install the browser's driver and have the binaries in your $PATH variable. Defaults parameters are host=http://192.168.1.4:4000 and browser=chrome

$ npm run storybook
* run a local server on port 9001 serving storybooks

```


## Work Flow rules

* We adopt gitFlow workFlow
* two branches that track project history: **master** & **development**
* **master** branch is updated only for important releases and marked with a versioning tag (`git tag`)
* all other work is done on **development**
* all branches are opened from **development** branch and merged back into it, the only possible branches sprouting from master are 'bugfix' branches (tagged 1.0.1)
* at the beginning and end of every day each dev control the 'branches' page on beanstalk to make sure their branch is up to date with master (no commits left behind)
* small changes like linting or small fixes are made directly on the **development** branch, so that every body sees them immediately
* branches must be 'folder specific' as much as possible (no changes outside of scope)
* **pull requests** must be reviewed (and eventually approved) within 24 hours from opening
