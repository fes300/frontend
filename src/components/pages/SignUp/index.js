import { connect } from 'react-redux'
import Presentational from './presentational'


const mapStateToProps = state => ({
  userName: state.auth.get('userName'),
  isAuthenticated: state.auth.get('isAuthenticated'),
  mainMessage: state.signUp.get('mainMessage'),
})

export default connect(mapStateToProps)(Presentational)
