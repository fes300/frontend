import React from 'react'
import { Link } from 'react-router'
import AppBar from 'react-toolbox/lib/app_bar'
import Drawer from 'components/atoms/Drawer'
import style from './style.css'


const Toolbar = ({ active, isAuthenticated, pathname, onClick, onLeftIconClick }) => (
  <AppBar
    className={style.theme}
    title="Tiplan"
    leftIcon="menu"
    onLeftIconClick={onLeftIconClick}
  >
    <Drawer
      active={active}
      onClick={onClick}
      child={
        <div className={style.navigation}>
          <Link className={style.link} to="/" onClick={onClick}>Home</Link>
          <Link className={style.link} to="/second" onClick={onClick}>Second</Link>
          {!isAuthenticated &&
            <Link className={style.link} to="/signup" onClick={onClick}>Signup</Link>}
          <Link className={style.link} to="/login" onClick={onClick}>
            {isAuthenticated ? 'Logout' : 'Login'}
          </Link>
        </div>
      }
    />
  </AppBar>
)


Toolbar.propTypes = {
  active: React.PropTypes.bool,
  pathname: React.PropTypes.string,
  onClick: React.PropTypes.func,
  onLeftIconClick: React.PropTypes.func,
}

export default Toolbar
