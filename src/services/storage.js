const Storage = (function selectStorage() {
  var _storage
  function createStorage() {
    const selectedStorage = {}
    try {
      localStorage.setItem('xxxtestStoragexxx', 'testStorage')
    } catch (e) {
      selectedStorage.setItem = setCookie
      selectedStorage.getItem = getCookie
      selectedStorage.removeItem = removeCookie
      return selectedStorage
    }
    localStorage.removeItem('xxxtestStoragexxx')
    return selectedStorage
  }

  return {
    getInstance: () => {
      if (!_storage) {
        _storage = createStorage()
      }
      return _storage
    },
  }
}())


function setCookie(name, value) {
  const cookie = `${name}=${value}`
  document.cookie = cookie
  return true
}

function getCookie(name) {
  const regExp = new RegExp(`(?:(?:^|.*;\\s*)${name}\\s*=\\s*([^;]*).*$)|^.*$`)
  const cookie = document.cookie.replace(regExp, '$1')
  return cookie
}

function removeCookie(sKey) {
  if (!hasCookie(sKey)) { return false }
  document.cookie = `${encodeURIComponent(sKey)}=; expires=Thu, 01 Jan 1970 00:00:00 GMT`
  return true
}

function hasCookie(sKey) {
  if (!sKey) { return false }
  return (
    new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")
  ).test(document.cookie)
}

const storage = Storage.getInstance()

export {
  storage,
  hasCookie,
  removeCookie,
  getCookie,
  setCookie,
}
