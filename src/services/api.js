import axios from 'axios'


const api = (function createApi() {
  let auth
  let ready
  let api

  function setAuth(token) {
    auth = token
    ready = false
    return buildApi()
  }

  function buildApi() {
    if (ready) return api
    api = axios.create({
      baseURL: 'http://localhost:4001',
      timeout: 10000,
      headers: {
        common: {
          Authorization: `Bearer ${auth}`,
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      },
    })
    return api
  }

  return {
    getInstance: () => buildApi(),
    setAuth: token => setAuth(token),
  }
}())

export default api
