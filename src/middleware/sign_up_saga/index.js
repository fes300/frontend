import { takeEvery } from 'redux-saga'
import { put } from 'redux-saga/effects'
import Api from 'services/api'
import { translateMongooseErrors, generalNetworkError } from 'utils'
import { SIGN_UP_REQUEST } from 'constants/actionTypes'
import { signUpSuccess, signUpFailure, authSuccess } from 'constants/actionCreators'


export default function* () {
  yield* takeEvery(
    SIGN_UP_REQUEST,
    request,
  )
}

function* request(action) {
  const { success, token, message, userName } = yield postData(action)
  if (success && token) {
    yield put(signUpSuccess({}))
    yield put(authSuccess({
      token,
      userName,
      isAuthenticated: true,
    }))
  } else {
    yield put(signUpFailure({
      mainMessage: message,
    }))
  }
}


function postData({ payload }) {
  const api = Api.getInstance()

  return api.post(
    '/users/user',
    {
      userName: payload.userNameState.value,
      email: payload.mailState.value,
      password: payload.passwordState.value,
    },
    { timeout: 120000 },
  ).then(onSuccess).catch(onError)
}


const onSuccess = response =>
  Object.assign(
    {},
    response.data,
    response.data.user,
    {
      status: response.status,
      message: getMessage(response),
    })


const onError = error =>
  generalNetworkError(error)


function getMessage(response) {
  if (response.status === 206 && response.data.errors) {
    const errors = [...Object.keys(response.data.errors)].map(field => ({
      field,
      kind: response.data.errors[field].kind,
    }))
    return translateMongooseErrors(errors[0].field)(errors[0].kind)
  }
  return `Ciao ${response.data.user.userName}, benvenuto in Tiplan :)`
}
