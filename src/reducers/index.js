import { combineReducers } from 'redux'
import { routerReducer as routing } from 'react-router-redux'

import auth from './auth_reducer'
import signUp from './sign_up_reducer'
import app from './app_reducer'


export default combineReducers({
  routing,
  auth,
  signUp,
  app,
})
