import init from './init'


export default function (app = init(), action) {
  switch (action.type) {
    case 'APP_CHANGE': {
      return app.mergeDeep(action.payload)
    }

    default: {
      return app
    }
  }
}
