import { createStore } from 'redux'
import reducers from '../reducers'
import { middleware, sagas, sagaMiddleware } from '../middleware'

const initialState = {}

const store = createStore(
    reducers,
    initialState,
    middleware,
  )

sagas.map(saga => sagaMiddleware.run(saga))

if (module.hot) {
  // Enable Webpack hot module replacement for reducers
  module.hot.accept('../reducers', () => {
    const nextRootReducer = require('../reducers/index')
    store.replaceReducer(nextRootReducer)
  })
}

export default store
