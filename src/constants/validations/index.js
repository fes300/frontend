import R from 'ramda'

/*
 * Presence
 */

export const validatePresence = (value) => {
  const message = 'Il campo è obbligatorio'
  const valid = !R.isEmpty(value) && !R.isNil(value)
  return (valid) ? null : message
}

/*
 * Only letters
 */

export const noNumbers = (value = '') => {
  const message = 'Il campo non può contenere caratteri numerici'
  const digits = value.match('[0-9]')
  return digits ? message : null
}

/*
 * No special characters
 */

export const noSpecialChars = (value = '') => {
  const message = 'Il campo non può contenere il carattere'
  const specialChars = value.match(/[^a-zA-ZàèìòùéóäëïöüçñÁÇâêôîû ']+/)
  return specialChars ? `${message} '${specialChars[0]}'` : null
}

/*
 * Email
 */

export const validateEmail = (value) => {
  const message = 'Formato email non valido (es. user@gmail.com)'
  const valid = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i.test(value) && !!value // eslint-disable-line
  return (valid) ? null : message
}

export const validatePassword = (value) => {
  let message = 'La password deve essere lunga almeno 6 caratteri'
  if (value.length < 6) return message
  message = 'La password deve contenere almeno un numero (0-9)!'
  let regExp = /[0-9]/
  if (!regExp.test(value)) return message
  message = 'La password deve contenere almeno una lettera in minuscolo (a-z)!'
  regExp = /[a-z]/
  if (!regExp.test(value)) return message
  message = 'La password deve contenere almeno una lettera in maiuscolo (A-Z)!'
  regExp = /[A-Z]/
  return !regExp.test(value) ? message : null
}
