import { connect } from 'react-redux'
import { signUpRequest, signUpSuccess, signUpFailure } from 'constants/actionCreators'
import { validateEmail, validatePassword } from 'constants/validations'
import Presentational from './presentational'


const mapStateToProps = state => state.signUp.toObject()

const mapDispatchToProps = dispatch => ({
  onSubmit: (event) => {
    event.preventDefault()
    const data = new FormData(event.target) // eslint-disable-line

    // validate userName
    const userName = data.get('userName')
    if (!userName) return dispatch(signUpFailure({
      userNameState: {
        message: 'il campo non può essere lasciato vuoto',
        status: 'error',
        error: true,
      },
      mainMessage: 'il campo "nome utente" non può essere lasciato vuoto',
    }))

    // validate mail
    const mail = data.get('mail')
    let message = validateEmail(mail)
    if (message) return dispatch(signUpFailure({
      mailState: {
        message,
        status: 'error',
        error: true,
      },
      mainMessage: message,
    }))

    // validate password
    const password = data.get('password')
    message = validatePassword(password)
    if (message) return dispatch(signUpFailure({
      passwordState: {
        message,
        status: 'error',
        error: true,
      },
      mainMessage: message,
    }))

    // validate confirmation password
    const confirmationPassword = data.get('confirmationPassword')
    message = confirmationPassword !== password
    if (message) {
      return dispatch(signUpFailure({
        confirmationPasswordState: {
          message: 'le password non combaciano',
          status: 'error',
          error: true,
        },
        mainMessage: 'le password non combaciano',
      }))
    }

    return dispatch(signUpRequest({
      userNameState: {
        message: 'lo userName è valido',
        status: 'success',
        value: userName,
        error: false,
      },
      mailState: {
        message: 'la mail è valida',
        status: 'success',
        value: mail,
        error: false,
      },
      passwordState: {
        message: 'la password è valida',
        status: 'success',
        value: password,
        error: false,
      },
      confirmationPasswordState: {
        message: 'le password combaciano',
        status: 'success',
        value: confirmationPassword,
        error: false,
      },
      mainMessage: null,
    }))
  },

  validateEmail: (value) => {
    const message = validateEmail(value)
    if (message) {
      return dispatch(signUpFailure({
        mailState: {
          message,
          status: 'error',
          value,
          error: true,
        },
        mainMessage: null,
      }))
    }
    return dispatch(signUpSuccess({
      mailState: {
        message: 'la mail è valida',
        status: 'success',
        value,
        error: false,
      },
      mainMessage: null,
    }))
  },

  validatePassword: (value) => {
    const message = validatePassword(value)
    if (message) {
      return dispatch(signUpFailure({
        passwordState: {
          message,
          status: 'error',
          value,
          error: true,
        },
        mainMessage: null,
      }))
    }
    return dispatch(signUpSuccess({
      passwordState: {
        message: 'la password è valida',
        status: 'success',
        value,
        error: false,
      },
      mainMessage: null,
    }))
  },

  validateConfirmationPassword: (value, password) => {
    const message = value !== password
    if (message) {
      return dispatch(signUpFailure({
        confirmationPasswordState: {
          message: 'le password non combaciano',
          status: 'error',
          value,
          error: true,
        },
        mainMessage: null,
      }))
    }
    return dispatch(signUpSuccess({
      confirmationPasswordState: {
        message: 'le password combaciano',
        status: 'success',
        value,
        error: false,
      },
      mainMessage: null,
    }))
  },

  validateUserName: value => dispatch(signUpSuccess({
    userNameState: {
      message: 'lo userName è valido',
      status: 'success',
      value,
      error: false,
    },
    mainMessage: null,
  })),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Presentational)
