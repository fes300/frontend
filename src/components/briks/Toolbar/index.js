import { connect } from 'react-redux'
import { appChange } from 'constants/actionCreators'
import Presentational from './presentational'


const mapStateToProps = state => ({
  pathname: state.routing.locationBeforeTransitions.pathname,
  active: state.app.get('menu').get('active'),
  isAuthenticated: state.auth.get('isAuthenticated'),
})

const mapDispatchToProps = dispatch => ({
  onClick: (active) => {
    dispatch(appChange({
      menu: { active: !active },
    }))
  },
})

const mergeProps = (mapStateProps, mapDispatchProps) => ({
  ...mapStateProps,
  ...mapDispatchProps,
  onLeftIconClick: mapDispatchProps.onClick.bind(undefined, mapStateProps.active),
})


export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(Presentational)
