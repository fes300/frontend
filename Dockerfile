FROM node:7.5.0

MAINTAINER federico sordillo <federicosordillo@gmail.com>

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN npm install

CMD [ "npm", "start" ]

EXPOSE 8080
