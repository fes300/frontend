import { baseUrl, By, expect, host, selenium, until, query, browser } from './constants/dependencies'
import * as tests from './tests'


console.log(`testing ${host} on ${browser}`)


before(function(done) {
  this.timeout(25000)
  this.driver = new selenium.Builder()
    .forBrowser(browser)
    .build()

  this.driver.getWindowHandle()

  this.driver.get(`${host}${baseUrl}${query}`)
  .then(() => done())
})

Object.keys(tests).forEach((test) => {
  tests[test]()
})

after(function(done) {
  this.driver.quit().then(() => done())
})
