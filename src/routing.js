import App from 'components/containers/App'


function errorLoading(err) {
  console.error('Dynamic page loading failed', err) // eslint-disable-line
}

function loadRoute(cb) {
  return module => cb(null, module.default)
}

export default process.env.NODE_ENV === 'development'
  ? {
    component: App,
    childRoutes: [
      { path: '/', component: require('components/pages/Home').default },
      { path: '/second', component: require('components/pages/Second').default },
      { path: '/login', component: require('components/pages/Login').default },
      { path: '/signup', component: require('components/pages/SignUp').default },
    ],
  }
  : {
    component: App,
    childRoutes: [{
      path: '/',
      getComponent(location, cb) {
        System.import('components/pages/Home')
          .then(loadRoute(cb))
          .catch(errorLoading)
      },
    }, {
      path: '/second',
      getComponent(location, cb) {
        System.import('components/pages/Second')
          .then(loadRoute(cb))
          .catch(errorLoading)
      },
    }, {
      path: '/login',
      getComponent(location, cb) {
        System.import('components/pages/Login')
          .then(loadRoute(cb))
          .catch(errorLoading)
      },
    }, {
      path: '/signup',
      getComponent(location, cb) {
        System.import('components/pages/SignUp')
          .then(loadRoute(cb))
          .catch(errorLoading)
      },
    }],
  }
