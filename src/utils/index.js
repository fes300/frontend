import R from 'ramda'
import ReactGA from 'react-ga'
import Tweezer from 'tweezer.js'


export function generalNetworkError(error) {
  if (error.response) {
    return Object.assign(
      {},
      error.response.data,
      {
        status: error.response.status,
        message: 'si è verificato un errore di rete',
      },
    )
  }
  return {
    message: 'sembra che non sia attiva la connessione internet...',
  }
}

export function translateMongooseErrors(errorField) {
  switch (errorField) {
    case 'email': {
      return translateError.bind(undefined, 'email')
    }
    case 'userName': {
      return translateError.bind(undefined, 'Nome utente')
    }
    default:
      return true
  }
}

function translateError(field, kind) {
  switch (kind) {
    case 'unique': {
      return `il campo ${field} è già utilizzato da un altro utente`
    }
    default:
      return true
  }
}

/*
 * return a random "name" key from the list elements, drawn with the specified % of probs
 * ex argument:
   [
     {name: "a", percentage: 15},
     {name: "b", percentage: 40},
     {name: "c", percentage: 45}
   ]
 */

export const randomChoice = (alternatives) => {
  const rand = getRandomInt(1, 100)

  // sort alternatives
  const sortedAlternatives = sortByPercentage(alternatives)

  // bench alternatives
  const benchedAlternatives = sortedAlternatives.reduce((cum, el) =>
    Object.assign({}, cum, {
      result: cum.result.concat([Object.assign({}, el, { bench: el.percentage + cum.tot })]),
      tot: cum.tot + el.percentage,
    }), { tot: 0, result: [] })

  if (benchedAlternatives.tot !== 100) {
    throw new Error(`"percentage" keys must sum up to 100, you gave ${benchedAlternatives.tot}`)
  }

  // compute results
  const computeResults = assignValues(rand)
  const results = computeResults(benchedAlternatives.result)

  // select the first valid
  return findFirstNotNull(results)
}

// randa helpers
const findFirstNotNull = R.find(a => a)
const sortByPercentage = R.sortBy(R.prop('percentage'))

const assignValues = (rand, alternatives) => {
  if (!alternatives) {
    return function curriedAssignValues(alternatives) {
      return alternatives.map(alternative =>
        (parseInt(alternative.bench, 10) >= rand ? alternative.name : null))
    }
  }
  return alternatives.map(alternative =>
    (parseInt(alternative.bench, 10) >= rand ? alternative.name : null))
}

function getRandomInt(min, max) {
  const realmin = Math.ceil(min)
  const realmax = Math.floor(max)
  return Math.floor(Math.random() * (realmax - realmin)) + realmin
}


/*
 * smoothscroll to element if present
 */

export function scrollTo(ref, duration = 1000, topOffset = 0, window) {
  let end

  if (ref instanceof HTMLElement) {
    end = ref.getBoundingClientRect().top - (topOffset + window.scrollY)
  }

  if (typeof ref === 'number') {
    end = ref + window.scrollY
  }

  new Tweezer({
    start: window.scrollY,
    end,
    duration,
  })
    .on('tick', v => window.scrollTo(0, v))
    .begin()
}


/* iterate a Promise N times, extending the given timeout every time.
 * takes 3 args: [N° of iterations, starting timeout(milliseconds), Promise]
 */

export async function iterateRequest(iterations, milliseconds, promise) {
  for (let i = 0; i < iterations; i += 1) {
    const increasedMilliseconds = milliseconds * (i + 1)
    const myTimeout = timeout(milliseconds)
    const myRace = createRace(increasedMilliseconds, promise, myTimeout)
    const { ok, response, error } = await myRace
    if (ok) return { ok, response }
    if (i === iterations - 1) return { ok, error }
  }
  return { ok: false, error: 'timeout' }
}


function createRace(milliseconds, promise, timeout) {
  return Promise.race([timeout, promise])
    .then(response => ({ ok: true, response }))
    .catch(error => ({ ok: false, error }))
}


/*
 * Transforms 1 to 01, 2 to 02, ...
 */

export const pad = d => (d < 10 ? `0${d.toString()}` : d.toString())


/*
 * returns a date Object at n days from a given date (default to today).
 */

export function addDays(days, startDate = new Date()) {
  return new Date(startDate.setDate(startDate.getDate() + days))
}


/*
 * returns the milliseconds rappresentation of a date object (net of timing).
 * Useful to compare dates.
 */

export function toMilliseconds(date) {
  date.setHours(0)
  date.setMinutes(0)
  date.setSeconds(0)
  date.setMilliseconds(0)
  return date.valueOf()
}

/*
 * compares two date object (net of timing).
 * returns true if they conform to the rule passed
 */

export function campareDate(sign, date1, date2) {
  switch (sign) {
    case '<=': {
      return toMilliseconds(date1) <= toMilliseconds(date2)
    }
    case '>=': {
      return toMilliseconds(date1) >= toMilliseconds(date2)
    }
    case '<': {
      return toMilliseconds(date1) < toMilliseconds(date2)
    }
    case '>': {
      return toMilliseconds(date1) > toMilliseconds(date2)
    }
    case '=': {
      return toMilliseconds(date1) === toMilliseconds(date2)
    }
    default:
      throw new Error(`comparison sign ${sign} is not valid`)
  }
}


/*
 * wait a few milliseconds then resolves to true
 */

export const delay = ms =>
  new Promise(resolve => setTimeout(() => resolve(true), ms))


/*
 * wait a few milliseconds then rejects to { timeout: true }
 */

export const timeout = ms =>
  new Promise((resolve, reject) => setTimeout(() => reject({ timeout: true }), ms))


/*
 * Creates a random alphanumeric string.
 *
 * @example
 *  utils.random('')
 *  → invv3nmf
 */

export const random = () => (new Date().getTime()).toString(36)


/*
  * get the path parameters
  *
  */

export const getUrlsQuery = (location) => {
  const query = location.search.length !== 0
  if (query) {
    const locationStringified = `{${location.search.replace('?', '"').replace(/=/gi, '": "').replace(/&/gi, '" , "')}"}`
    const queryOBJ = JSON.parse(locationStringified)
    return queryOBJ
  }
  return {}
}


/*
 * Google analytics
 * - set the tracking code
 */

ReactGA.initialize('UA-12480037-1', {
  // gaOptions: { name: 'auto' }
})

export const ga = (pathname) => {
  // ReactGA.set({ page: pathname, anonymizeIp: true })
  // ReactGA.set({ page: pathname })
  ReactGA.pageview(pathname)
}


/* nice snippet to remember the true nature of async/await functions, and that generators are the real shit

  function request(string) {
      return new Promise((resolve,reject) => {
       setTimeout(() => resolve(string), 1000)
      })
  }

  function runGenerator(g) {
      var it = g(), ret;

      (function iterate(val){
          ret = it.next( val );

          if (!ret.done) {
              if ("then" in ret.value) {
                  // wait on the promise
                  ret.value.then( iterate );
              }
          }
      })();
  }

  runGenerator( function *main(){
      var result1 = yield request( "ciao1" );
      var result2 = yield request( "ciao2" );
      console.log( "The value you asked for: " + result1 + result2 );
  });

*/
