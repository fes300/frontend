export const selenium = require('selenium-webdriver')
export const test = require('selenium-webdriver/testing')
export const chai = require('chai')
chai.use(require('chai-as-promised'))

export const until = selenium.until
export const By = selenium.By

// initial host can be set from CLI with the parameter host=[...]
export const host = process.env.npm_config_host
  ? process.env.npm_config_host
  : 'http://localhost:8080'

// browser to use for testing can be set from CLI with the parameter browser=[...]
export const browser = process.env.npm_config_browser
  ? process.env.npm_config_browser
  : 'chrome'

// where to start
export const baseUrl = '/#'
export const query = ''

export const promise = t => new Promise(resolve =>
  setTimeout(() => resolve(true), t),
)
export const timeout = t => new Promise((resolve) => {
  setTimeout(resolve, t, 'chiarezza timeout')
})

export const expect = chai.expect
