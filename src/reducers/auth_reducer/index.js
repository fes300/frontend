import Api from 'services/api'
import init from './init'


export default function (auth = init(), action) {
  switch (action.type) {
    case 'AUTH_SUCCESS': {
      if (action.payload.token) Api.setAuth(action.payload.token)
      return auth.mergeDeep(action.payload)
    }
    case 'AUTH_FAILURE': {
      return auth.mergeDeep(action.payload)
    }
    case 'LOGOUT_REQUEST': {
      return init()
    }
    default: {
      return auth
    }
  }
}
