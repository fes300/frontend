import React from 'react'
import Input from 'components/atoms/Input'
import { Button } from 'react-toolbox/lib/button'
import style from './style.css'


const LoginForm = ({
  userNameState,
  passwordState,
  validateUserName,
  validatePassword,
  onSubmit,
}) => (
  <form
    onSubmit={onSubmit}
  >
    <Input
      error={userNameState && userNameState.get('error') && userNameState.get('message')}
      label="nome utente"
      name="userName"
      onChange={value => validateUserName(value)}
      status={userNameState && userNameState.get('status')}
      value={userNameState && userNameState.get('value')}
    />
    <Input
      error={passwordState && passwordState.get('error') && passwordState.get('message')}
      label="password"
      name="password"
      status={passwordState && passwordState.get('status')}
      type="password"
      onChange={value => validatePassword(value)}
      value={passwordState && passwordState.get('value')}
    />
    <Button
      className={style.loginButton}
      label="login"
      type="submit"
      raised
    />
  </form>
)

LoginForm.propTypes = {
  onSubmit: React.PropTypes.func,
  userNameState: React.PropTypes.shape({
    message: React.PropTypes.bool,
    status: React.PropTypes.string,
    value: React.PropTypes.string,
  }),
  passwordState: React.PropTypes.shape({
    message: React.PropTypes.bool,
    status: React.PropTypes.string,
    value: React.PropTypes.string,
  }),
  validateUserName: React.PropTypes.func.isRequired,
  validatePassword: React.PropTypes.func.isRequired,
}

export default LoginForm
