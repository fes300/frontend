import R from 'ramda'
import { expect, By, promise } from '../../constants/dependencies'


/* eslint-disable no-undef */
export default ({ double, waitAtTheEnd }) => {
  it('refreshing does not change the values in input fields', async function itTest() {
    this.timeout(60000)

    await this.driver.navigate().back()
    if (double) {
      await this.driver.navigate().back()
    }
    const step = await this.driver.executeScript(() => window.location.hash.split('?')[0])

    await promise(500)
    const originalValues = await getInputValues(this.driver)

    await this.driver.navigate().refresh()
    await this.driver.manage().timeouts().pageLoadTimeout(10000)

    const refreshedStep = await this.driver.executeScript(() => window.location.hash.split('?')[0])

    // if you have been redirected to the last step you modified (as you should have), go back to the one we want to check
    if (refreshedStep !== step) await this.driver.navigate().back()

    const refreshedValues = await getInputValues(this.driver)
    await this.driver.navigate().forward()
    if (double) {
      await this.driver.navigate().forward()
    }

    if (waitAtTheEnd) {
      await promise(waitAtTheEnd)
    }


    for (let i = 0; i < originalValues.length; i += 1) {
      expect(originalValues[i]).to.deep.equal(refreshedValues[i])
    }
  })
}


const getInputValues = async (driver) => {
  const inputs = await driver.findElements(By.css('input'))
  const names = await Promise.all(inputs.map(async input =>
    input.getAttribute('name'),
  ))
  const values = await Promise.all(inputs.map(async input =>
    input.getAttribute('value'),
  ))

  return R.filter(
    input => input.value,
    names.map((name, index) => (
      { name, value: values[index] }
    )),
  )
}
