import { configure } from '@kadira/storybook';


const req = require.context('../app/Components', true, /story\.js$/)

function loadStories() {
  req.keys().forEach(req)
  require('./index.scss')
}

configure(loadStories, module);
