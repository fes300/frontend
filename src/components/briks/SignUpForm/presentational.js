import React from 'react'
import Input from 'components/atoms/Input'
import { Button } from 'react-toolbox/lib/button'
import style from './style.css'


const LoginForm = ({
  confirmationPasswordState,
  mailState,
  passwordState,
  userNameState,
  validateEmail,
  validatePassword,
  validateConfirmationPassword,
  validateUserName,
  onSubmit,
}) => (
  <form
    onSubmit={onSubmit}
  >
    <Input
      error={userNameState && userNameState.get('error') && userNameState.get('message')}
      label="nome utente"
      name="userName"
      status={userNameState && userNameState.get('status')}
      value={userNameState && userNameState.get('value')}
      onChange={value => validateUserName(value)}
    />
    <Input
      error={mailState && mailState.get('error') && mailState.get('message')}
      label="email"
      name="mail"
      status={mailState && mailState.get('status')}
      type="email"
      value={mailState && mailState.get('value')}
      onChange={value => validateEmail(value)}
    />
    <Input
      error={passwordState && passwordState.get('error') && passwordState.get('message')}
      label="password"
      name="password"
      status={passwordState && passwordState.get('status')}
      type="password"
      value={passwordState && passwordState.get('value')}
      onChange={value => validatePassword(value)
        && validateConfirmationPassword(
          confirmationPasswordState && confirmationPasswordState.get('value'),
          value,
        )
      }
    />
    <Input
      hint="riscrivi la password scelta"
      error={confirmationPasswordState && confirmationPasswordState.get('error') && confirmationPasswordState.get('message')}
      name="confirmationPassword"
      status={confirmationPasswordState && confirmationPasswordState.get('status')}
      type="password"
      value={confirmationPasswordState && confirmationPasswordState.get('value')}
      onChange={value => validateConfirmationPassword(value, passwordState && passwordState.get('value'))}
    />
    <Button
      className={style.signUpButton}
      label="Sign Up"
      type="submit"
      raised
    />
  </form>
)

LoginForm.propTypes = {
  onSubmit: React.PropTypes.func,
  confirmationPasswordState: React.PropTypes.shape({
    message: React.PropTypes.bool,
    status: React.PropTypes.string,
    value: React.PropTypes.string,
  }),
  mailState: React.PropTypes.shape({
    message: React.PropTypes.bool,
    status: React.PropTypes.string,
    value: React.PropTypes.string,
  }),
  passwordState: React.PropTypes.shape({
    message: React.PropTypes.bool,
    status: React.PropTypes.string,
    value: React.PropTypes.string,
  }),
  userNameState: React.PropTypes.shape({
    message: React.PropTypes.bool,
    status: React.PropTypes.string,
    value: React.PropTypes.string,
  }),
  validateEmail: React.PropTypes.func.isRequired,
  validatePassword: React.PropTypes.func.isRequired,
  validateConfirmationPassword: React.PropTypes.func.isRequired,
  validateUserName: React.PropTypes.func.isRequired,
}

export default LoginForm
