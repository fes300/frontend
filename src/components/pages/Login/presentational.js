import React from 'react'
import LoginForm from 'components/briks/LoginForm'
import LogOutPrompt from 'components/briks/LogOutPrompt'

const Login = ({
  isAuthenticated,
  userName,
  mainMessage,
}) => (
  <div>
    {isAuthenticated
      ? <LogOutPrompt userName={userName} />
      : (
        <div>
          <span>{mainMessage}</span>
          <LoginForm />
        </div>
      )
    }
  </div>
)

Login.propTypes = {
  isAuthenticated: React.PropTypes.bool.isRequired,
  mainMessage: React.PropTypes.string,
  userName: React.PropTypes.string,
}

export default Login
