import React from 'react'
import { Provider } from 'react-redux'

import { Router, hashHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import store from './store'
import rootRoute from './routing'

const history = syncHistoryWithStore(hashHistory, store)

export default React.createClass({ // eslint-disable-line
  render: () => (
    <Provider store={store}>
      <Router history={history} routes={rootRoute} />
    </Provider>
  ),
})
