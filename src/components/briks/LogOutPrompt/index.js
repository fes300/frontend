import { connect } from 'react-redux'
import { logoutRequest } from 'constants/actionCreators'
import Presentational from './presentational'


const mapDispatchToProps = dispatch => ({
  onClick: (event) => {
    event.preventDefault()
    dispatch(logoutRequest())
  },
})

export default connect(
  () => ({}),
  mapDispatchToProps,
)(Presentational)
