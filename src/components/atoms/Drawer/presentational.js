import React from 'react'
import Drawer from 'react-toolbox/lib/drawer'
import style from './style.css'

const DrawerTest = ({ active, onOverlayClick, child }) => (
  <Drawer
    active={active}
    className={style.theme}
    onOverlayClick={onOverlayClick}
  >
    {child}
  </Drawer>
)

DrawerTest.propTypes = {
  active: React.PropTypes.bool,
  child: React.PropTypes.node,
  onOverlayClick: React.PropTypes.func,
}

export default DrawerTest
