import { takeEvery } from 'redux-saga'
import { put } from 'redux-saga/effects'
import Api from 'services/api'
import { push } from 'react-router-redux'
import { translateMongooseErrors, generalNetworkError } from 'utils'
import { AUTH_REQUEST } from 'constants/actionTypes'
import { authSuccess, authFailure } from 'constants/actionCreators'


export default function* () {
  yield* takeEvery(
    AUTH_REQUEST,
    request,
  )
}


function* request(action) {
  const { success, token, message, userName } = yield postData(action)
  if (success && token) {
    Api.setAuth(token)
    yield put(authSuccess({
      token,
      userName,
      isAuthenticated: true,
    }))
    // redirect to homepage after login
    yield put(push({ pathname: '/' }))
  } else {
    yield put(authFailure({
      mainMessage: message,
    }))
  }
}


function postData({ payload }) {
  const api = Api.getInstance()

  return api.post(
    '/sessions/create',
    {
      userName: payload.userNameState.value,
      password: payload.passwordState.value,
    },
    { timeout: 120000 },
  ).then(onSuccess).catch(onError)
}


const onSuccess = response =>
  Object.assign(
    {},
    response.data,
    response.data.user,
    {
      status: response.status,
      message: getMessage(response),
    })


const onError = (error) => {
  if (error.response && error.response.status === 404 && error.response.data) {
    return {
      message: 'le credenziali non risultano corrette',
    }
  }
  return generalNetworkError(error)
}


function getMessage(response) {
  if (response.status === 206 && response.data.errors) {
    const errors = [...Object.keys(response.data.errors)].map(field => ({
      field,
      kind: response.data.errors[field].kind,
    }))
    return translateMongooseErrors(errors[0].field)(errors[0].kind)
  }
  return `Ciao ${response.data.userName}, benvenuto in Tiplan :)`
}
