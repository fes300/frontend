import * as types from './actionTypes'


/* AUTH */
export const authRequest = payload => ({
  type: types.AUTH_REQUEST,
  payload,
})

export const authSuccess = payload => ({
  type: types.AUTH_SUCCESS,
  payload,
})

export const authFailure = payload => ({
  type: types.AUTH_FAILURE,
  payload,
})


/* SIGN-UP */
export const signUpRequest = payload => ({
  type: types.SIGN_UP_REQUEST,
  payload,
})

export const signUpSuccess = payload => ({
  type: types.SIGN_UP_SUCCESS,
  payload,
})

export const signUpFailure = payload => ({
  type: types.SIGN_UP_FAILURE,
  payload,
})


/* LOGOUT */
export const logoutRequest = payload => ({
  type: types.LOGOUT_REQUEST,
  payload,
})

export const logoutSuccess = payload => ({
  type: types.LOGOUT_SUCCESS,
  payload,
})

export const logoutFailure = payload => ({
  type: types.LOGOUT_FAILURE,
  payload,
})


/* APP */
export const appChange = payload => ({
  type: types.APP_CHANGE,
  payload,
})
