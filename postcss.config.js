const path = require('path')


module.exports = {
  plugins: {
    'postcss-import': {
      root: path.resolve(__dirname),
      path: path.resolve(__dirname, 'src', 'style'),
    },
    'postcss-mixins': {},
    'postcss-each': {},
    'postcss-cssnext': {},
    'postcss-reporter': {},
  },
}
