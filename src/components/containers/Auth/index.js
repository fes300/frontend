import React from 'react'
import { connect } from 'react-redux'
import LoginPage from 'components/pages/Login'


/* eslint-disable */
export function requireAuthentication({ Component, UnauthComponent, showLogin }) {
  class AuthenticatedComponent extends React.Component {
    render() {
      return (
        <div>
          {this.props.isAuthenticated === true
            ? <Component {...this.props} />
            : <div>
                <UnauthComponent />
                {showLogin ? <LoginPage /> : null}
              </div>
          }
        </div>
      )
    }
  }

  const mapStateToProps = state => ({
    userName: state.auth.get('userName'),
    isAuthenticated: state.auth.get('isAuthenticated')
  })

  return connect(mapStateToProps)(AuthenticatedComponent)
}
