import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import React from 'react'
import App from './app'
// AppContainer is a necessary wrapper component for HMR


const render = (Component) => {
  ReactDOM.render(
    <AppContainer>
      <Component />
    </AppContainer>,
    document.getElementById('root'),
  )
}

render(App)

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept('./app', () => {
    render(App)
  })
}

// register serviceWorker
if (navigator.serviceWorker) {
  require('offline-plugin/runtime').install()
}

// trick to adjust page height
document.addEventListener('DOMContentLoaded', () => {
  const h = screen.height
  document.querySelector('body').style.minHeight = `${h}px`
})
