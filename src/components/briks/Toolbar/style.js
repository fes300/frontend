export default {
  active: {
    color: 'blue',
    backgroundColor: '#fff',
  },
  link: {
    textDecoration: 'none',
    color: 'inherit',
  },
}
