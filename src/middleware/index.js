import { applyMiddleware } from 'redux'
import { routerMiddleware as routing } from 'react-router-redux'
import { hashHistory } from 'react-router'
import createSagaMiddleware from 'redux-saga'
import auth from './auth_saga'
import signUp from './sign_up_saga'

export const sagaMiddleware = createSagaMiddleware()

export const middleware = applyMiddleware(
  sagaMiddleware,
  routing(hashHistory),
)

export const sagas = [
  auth,
  signUp,
]
