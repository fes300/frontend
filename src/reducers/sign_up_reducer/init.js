import { Map } from 'immutable'


const emptySignup = {}

export default (signup = Map(emptySignup)) => signup
