import init from './init'


export default function (signup = init(), action) {
  switch (action.type) {
    case 'SIGN_UP_SUCCESS': {
      return signup.mergeDeep(action.payload)
    }
    case 'SIGN_UP_FAILURE': {
      return signup.mergeDeep(action.payload)
    }
    default: {
      return signup
    }
  }
}
