import { connect } from 'react-redux'
import Presentational from './presentational'


const mapDispatchToProps = (dispatch, props) => ({
  onOverlayClick: () => props.onClick(props.active),
})

export default connect(
  () => ({}),
  mapDispatchToProps,
)(Presentational)
