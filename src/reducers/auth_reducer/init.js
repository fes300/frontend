import { Map } from 'immutable'


const emptyAuth = {
  token: null,
  userName: null,
  isAuthenticated: false,
}

export default (auth = Map(emptyAuth)) => auth
