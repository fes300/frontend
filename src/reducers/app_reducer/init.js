import { Map } from 'immutable'


const emptyApp = {
  menu: Map({
    active: false,
  }),
}

export default (app = Map(emptyApp)) => app
