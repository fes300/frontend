const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const OfflinePlugin = require('offline-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')


const loaders = {
  css: conf => ({
    loader: 'css-loader',
    options: Object.assign({}, {
      modules: true,
      minimize: true,
      sourceMap: true,
      localIdentName: '[name]--[local]--[hash:base64:8]',
    }, conf),
  }),
  postcss: conf => ({
    loader: 'postcss-loader',
    options: Object.assign({}, {
      sourceMap: true,
    }, conf),
  }),
}


module.exports = {
  devtool: 'eval',

  entry: [
    'babel-polyfill',
    path.resolve(__dirname, 'src', 'index.js'),
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },

  context: path.resolve(__dirname, 'src'),

  plugins: [
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.DefinePlugin({
      __DEVTOOLS__: false,
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
    new HtmlWebpackPlugin({
      template: 'index.html',
    }),
    new ExtractTextPlugin({
      filename: 'bundle.css',
      disable: false,
      allChunks: true,
    }),
    new OfflinePlugin({
      responseStrategy: 'network-first',
      updateStrategy: 'all',
      AppCache: false,
    }),
  ],

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          plugins: ['react-hot-loader/babel'],
        },
      },
      {
        test: /\.css$/,
        include: [
          /(node_modules)\/react-toolbox/,
          path.resolve(__dirname, 'src'),
        ],
        loader: ExtractTextPlugin.extract({
          loader: [loaders.css({ importLoaders: 1 }), loaders.postcss()],
        }),
      },
      {
        test: /\.json$/,
        use: 'json-loader',
      },
    ],
  },

  resolve: {
    modules: [
      path.resolve(__dirname, 'src'),
      'node_modules',
      path.resolve(__dirname, 'node_modules'),
    ],
  },
}
