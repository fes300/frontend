const path = require('path')
const webpack = require('webpack')

module.exports = {

  entry: [
    'webpack-dev-server/client?http://127.0.0.1:9001',
    'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000',
  ],

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
  ],

  module: {
    loaders: [{
      test: /\.jsx?$/,
      loader: 'babel',
      include: path.join(__dirname, 'app'),
      query: {
        cacheDirectory: true,
        plugins: ['transform-runtime'],
        presets: ['es2015', 'react', 'stage-0'],
        env: {
          development: {
            plugins: [
              ['react-transform', {
                transforms: [{
                  transform: 'react-transform-hmr',
                  imports: ['react'],
                  locals: ['module'],
                }],
              }],
            ],
          },
        },
      },
    }, {
      test: /\.scss$/,
      loaders: ['style', 'css', 'sass'],
    }],
  },

  resolve: {
    extensions: ['', '.js', '.jsx'],
    root: [
      path.resolve(__dirname, '../app/Components'),
    ],
  },
}
